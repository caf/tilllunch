TillLunch: fetch lunch menus
=============================

This simple Python program fetches from the web information about
lunch menus in a few restaurants in the area around the campus of
Chalmers University of Technology in Gothenburg, Sweden.

Installation and dependencies
-----------------------------

TillLunch can be installed by running:

```bash
$ python2 setup.py install --user
```

This will fetch the dependencies [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) and [Selenium](http://selenium-python.readthedocs.io). While BeautifulSoup works out of the box, Selenium has a few more dependencies:

   1. Make sure a supported browser is installed (I use Firefox).
   
   2. Install a suitable driver (for
      [Firefox](https://github.com/mozilla/geckodriver/releases)), by
      downloading the binary for your architecure and putting it in a
      directory in the system's path (I use `/usr/local/bin`).

TillLunch uses [CutyCapt](http://cutycapt.sourceforge.net/) to perform
simple Facebook scraping, and `pdftotext` to convert PDF
sources. Install CutyCapt with:

```bash
$ apt install cutycapt
```

whereas `pdftotext` should be available in most Linux distributions.

After installation, TillLunch can be used by running `tillunch`.


## Redirecting TillLunch's output

TillLunch uses Unicode strings, which may trigger errors (`UnicodeEncodeError` exceptions) when redirecting its output to file in the shell.
A simple solution in Linux is to set the environment variable `PYTHONIOENCODING`

```bash
export PYTHONIOENCODING="UTF-8"
```

before calling `tillunch`.


Usage examples
--------------

The command-line help, accessible with the `-h` option, should be self-explanatory. 

A few examples:

   * Fetch today's menus of all available restaurants:

      ```
	  $ tillunch.py
      ```
	 
   * Fetch the Wednesday menu of restaurants OOTO and Einstein:

    ```
	 $ tillunch.py -d 2 OOTO Einstein
	 ```
	 
   * Fetch today's menus of all available restaurants *but* Mr. P,
     showing the URLs of the fetched pages:

     ```
	 $ tillunch.py -v --url MrP
	 ```


Environment
------------

TillLunch has been written and tested under Ubuntu 16.04 with Python
2.7. Your mileage may vary.

Fetching menus that rely on Selenium may only work on the second
attempt (that is, you have to run TillLunch twice): the first attempt
times out before the page is completely loaded, while further attempts
run faster (probably because of caching).
