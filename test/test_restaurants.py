# -*- coding: utf-8 -*-
import unittest
import tillunch.restaurants
import sys, os.path
import types


class Driver(object):

    def __init__(self, restaurant, days = range(0,5)):
        self.restaurant = restaurant
        self.days = days
        this_module = sys.modules[__name__]
        this_dir = os.path.dirname(os.path.abspath(this_module.__file__))
        self.this_dir = this_dir

    def file_url(self, fname):
        path = "file://" + os.path.join(self.this_dir, fname)
        return path

    def inject_urls(self, obj):
        fixed_url = lambda o: [self.file_url(self.restaurant + ".html")]
        obj.urls = types.MethodType(fixed_url, obj)
        return obj

    def inject_souped(self, obj):
        return obj

    def process(self, build_expect = False):
        for day in self.days:
            mc = getattr(tillunch.restaurants, self.restaurant)
            mo = mc(day)
            mo.set_layout("full")
            mo.include_url = False
            mo.include_day = True
            # inject dependencies
            mo = self.inject_urls(mo)
            mo = self.inject_souped(mo)
            mo.get_menu()
            fexp = self.restaurant + ".%d" % day + ".expect"
            fexp = os.path.join(self.this_dir, fexp)
            if build_expect:
                with open(fexp, "w") as fp:
                    fp.write(mo.text().encode("utf-8"))
                    expect = mo.text()
            else:
                with open(fexp, "r") as fp:
                    expect = fp.read().decode("utf-8")
            yield (mo.text(), expect)


class LocalReadDriver(Driver):

    def inject_souped(self, obj):
        super_souped = lambda o: super(obj.__class__, obj).souped()
        obj.souped = types.MethodType(super_souped, obj)
        return obj


class TestRestaurants(unittest.TestCase):

    def test_Wijkanders(self):
        runner = Driver("Wijkanders")
        for result, expect in runner.process():
            self.assertEqual(result, expect)

    def test_Einstein(self):
        runner = Driver("Einstein")
        for result, expect in runner.process():
            self.assertEqual(result, expect)

    def test_OOTO(self):
        runner = Driver("OOTO")
        for result, expect in runner.process():
            self.assertEqual(result, expect)

    def test_MrP(self):
        runner = Driver("MrP")
        for result, expect in runner.process():
            self.assertEqual(result, expect)
            
    def test_Thai(self):
        runner = Driver("Thai")
        for result, expect in runner.process():
            self.assertEqual(result, expect)

    def test_Linsen(self):
        runner = Driver("Linsen")
        for result, expect in runner.process():
            self.assertEqual(result, expect)
        
    def test_IndianBBQ(self):
        runner = LocalReadDriver("IndianBBQ")
        for result, expect in runner.process():
            self.assertEqual(result, expect)

    def test_Bistro(self):
        runner = LocalReadDriver("Bistro")
        for result, expect in runner.process():
            self.assertEqual(result, expect)

