# -*- coding: utf-8 -*-


class Menu(object):
    
    def __init__(self, on_day):
        self.on_day = on_day
        self.menu_day = u""
        self.menu = []
        self.restaurant = u""
        self.url = None

    def url(self):
        """
        Return a list of guessed URLs including self's menu on day
        self.on_day, ordered from the most likely.
        """
        return []

    def souped(self):
        """
        Return an iterator over BeautifulSoup objects, each a parsed
        tree object of self.urls().
        """
        import urllib2
        from bs4 import BeautifulSoup
        urls = self.urls()
        for url in urls:
            self.url = url
            page = urllib2.urlopen(url)
            soup = BeautifulSoup(page, "html.parser")
            yield soup

    def get_menu(self):
        """
        Retrieve the menu, and store it in self.
        """
        for soup in self.souped():
            if self.find_menu(soup):
                self.clean_menu()
                return
        self.menu = None

    def clean_menu(self):
        self.menu_day = self.menu_day.strip()
        # strip and remove multiple white characters and empty lines
        self.menu = [" ".join(s.strip().split()) \
                     for s in self.menu if s.strip() != u""]

    def find_menu(self, soup):
        """
        If soup contains a valid menu, store it in self and return True;
        otherwise return False.
        """
        return False


class PrintableMenu(Menu):
    
    LAYOUTS = ["full", "compact"]

    def __init__(self, on_day):
        super(PrintableMenu, self).__init__(on_day)
        self.set_layout(self.LAYOUTS[0])
        self.include_url = False
        self.include_day = True

    def set_layout(self, layout):
        """Set self.layout to layout."""
        if layout not in self.LAYOUTS:
            raise ValueError(layout + " is not a valid layout")
        else:
            self.layout = layout

    def layout_compact(self):
        """Unicode string of self's menu using compact layout."""
        res = []
        res += [u"*" + self.restaurant + u"*:"]
        if self.include_url and self.url is not None:
            res += [u"(" + self.url + u")"]
        if self.include_day and self.menu_day is not None:
            res += [self.menu_day]
        if self.menu is not None:
            res += self.menu
        return u" ".join(res)

    def layout_full(self):
        """Unicode string of self's menu using full layout."""
        res = []
        res += [self.restaurant, u"-----------------"]
        if self.include_url and self.url is not None:
            res += [u"(" + self.url + u")"]
        if self.include_day and self.menu_day is not None:
            res += [self.menu_day]
        if self.menu is not None:
            res += self.menu
        else:
            res += [u"[Could not retrieve menu]"]
        return u"\n".join(res)

    def text(self):
        """Unicode string of self's menu using self.layout."""
        layout_m = getattr(self, "layout_" + self.layout)
        return layout_m()

    def __str__(self):
        return self.text()
        
    def __unicode__(self):
        return str(self)
