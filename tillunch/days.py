# -*- coding: utf-8 -*-

days = {"english":
        [u"monday",
         u"tuesday",
         u"wednesday",
         u"thursday",
         u"friday",
         u"saturday",
         u"sunday"],
        "swedish":
        [u"måndag",
         u"tisdag",
         u"onsdag",
         u"torsdag",
         u"fredag",
         u"lördag",
         u"söndag"]}

def today():
    import datetime
    return datetime.datetime.today().weekday()

def day_name(n_day, language):
    return days[language][n_day]
